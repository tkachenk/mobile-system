module.exports = (sequelize, Sequelize) => {
  const Gender = sequelize.define("gender", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  })
  return Gender
}
