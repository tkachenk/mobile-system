module.exports = (sequelize, Sequelize) => {
  const Tarif = sequelize.define("tarif", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  })
  return Tarif
}
