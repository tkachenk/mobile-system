const Sequelize = require('sequelize')

const sequelize = new Sequelize("mobile", "admin", "admin", {
  dialect: "mysql",
  host: "mysql",
  charset: 'utf8',
  collate: 'utf8_general_ci'
})

admin = require('./admin.js')(sequelize, Sequelize)
user = require('./user.js')(sequelize, Sequelize)
gender = require('./gender.js')(sequelize, Sequelize)
tarif = require('./tarif.js')(sequelize, Sequelize)
city = require('./city.js')(sequelize, Sequelize)
messenger = require('./messenger.js')(sequelize, Sequelize)
userMessenger = require('./userMessenger.js')(sequelize, Sequelize)
statistic = require('./statistic.js')(sequelize, Sequelize)

gender.hasMany(user)
user.belongsTo(gender)

tarif.hasMany(user)
user.belongsTo(tarif)

city.hasMany(user)
user.belongsTo(city)

user.belongsToMany(messenger, {through: userMessenger});
messenger.belongsToMany(user, {through: userMessenger});

module.exports = {
  sequelize,
  Sequelize,
  user,
  gender,
  tarif,
  admin,
  city,
  messenger,
  userMessenger,
  statistic
}
