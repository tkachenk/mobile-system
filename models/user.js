module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    fio: {
      type: Sequelize.STRING
    },
    age: {
      type: Sequelize.INTEGER
    },
    phone: {
      type: Sequelize.STRING
    }
  })

  return User
}
