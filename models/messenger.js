module.exports = (sequelize, Sequelize) => {
  const Statistic = sequelize.define("messenger", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  });
  return Statistic
}

