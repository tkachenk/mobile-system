module.exports = (sequelize, Sequelize) => {
  const Statistic = sequelize.define("statistic", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    age: {
      type: Sequelize.INTEGER
    },
    price: {
      type: Sequelize.FLOAT
    },
    otherCity: {
      type: Sequelize.INTEGER
    },
    message: {
      type: Sequelize.INTEGER
    }
  }, {
    timestamps: false
  });
  return Statistic
}

