module.exports = (sequelize, Sequelize) => {
  const City = sequelize.define("city", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  })

  return City
}
