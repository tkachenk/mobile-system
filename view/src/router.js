import VueRouter from "vue-router";

const AuthPage = () => import('./components/AuthPage')
const CabinetPage = () => import('./components/CabinetPage')
const UsersView = () => import('./components/UsersView')
const TarifView = () => import('./components/TarifView')
const CityView = () => import('./components/CityView')
const MessengerView = () => import('./components/MessengerView')
const ExportView = () => import('./components/ExportView')
const StatisticView = () => import('./components/StatisticView')


export default new VueRouter({
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
    {
      path: '/auth',
      component: AuthPage
    },
    {
      path: '/',
      redirect: '/users-view',
      component: CabinetPage,
      children: [
        {
          path: 'users-view',
          component: UsersView
        },
        {
          path: 'tarif-view',
          component: TarifView
        },
        {
          path: 'city-view',
          component: CityView
        },
        {
          path: 'messenger-view',
          component: MessengerView
        },
        {
          path: 'export-view',
          component: ExportView
        },
        {
          path: 'statistic-view',
          component: StatisticView
        }
      ]
    }
  ]
})
