import router from "../router";

export default function http (method, url, bodyData) {
  const baseURL = 'http://localhost:8000/'
  const body = bodyData ? JSON.stringify(bodyData) : null
  const accessToken = localStorage.getItem('accessToken')
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${accessToken}`
  }

  return fetch(baseURL + url, {
    method,
    headers,
    body
  })
    .then(res => {
      if (res.url.indexOf('export') >= 0) {
        return res.blob()
      }
      return res.json()
    })
    .then(res => {
      if (res.status === 401) {
        localStorage.removeItem('accessToken')
        router.push('/auth')
      }
      return res
    })
    .catch(err => {
      console.error(err)
    })
}
