const express = require('express')
const app = express()
const port = 8000
const bodyParser = require('body-parser')
const cors = require('cors')
const jwt = require('jsonwebtoken')
const xl = require('excel4node')
const kmeans = require('node-kmeans')
const sd = require('ml-array-standard-deviation')
const mean = require('mean')
const cloneDeep = require('lodash/cloneDeep')
const groupBy = require('lodash/groupBy')
const {
  sequelize,
  user,
  gender,
  tarif,
  admin,
  city,
  messenger,
  statistic
} = require('./models/index')
let lastClusterData = null

app.use(bodyParser.json())

app.use(cors({
  origin: '*'
}))

sequelize.sync({alter: true})
  .then(() => {
    app.listen(port, () => {
      console.log(`Example app listening at http://localhost:${port}`)
    })
  })
  .catch(err => console.log(err))

const accessTokenSecret = 'secret'
const authenticateJWT = (req, res, next) => {
  const cookie = req.headers.cookie
  const authHeader = cookie.split(';').find(v => v.split('=')[0].trim() === 'auth').split('=')[1]
  console.log(authHeader)
  if (authHeader) {
    const token = authHeader.split(' ')[1]

    jwt.verify(token, accessTokenSecret, (err, user) => {
      if (err) {
        return res.json({
          message: 'Unauthorized',
          status: 401
        })
      }

      req.user = user
      next()
    })
  } else {
    res.json({
      message: 'Unauthorized',
      status: 401
    })
  }
}

app.get('/', authenticateJWT, (req, res) => {
  console.log(req)
  res.json({hi: true})
})

app.post('/login', (req, res) => {
  const { username, password } = req.body
  if (!username || !password) {
    return res.sendStatus(422)
  }

  admin.findAll()
    .then(result => {
      const user = result.find(u => { return u.username === username && u.password === password })
      if (user) {
        const accessToken = jwt.sign({ username: user.username }, accessTokenSecret)
        res.json({accessToken})
      } else {
        res.json({message: 'Username or password incorrect'})
      }
    })
    .catch(err => {
      res.json({status: 500})
    })
})

app.get('/users', authenticateJWT, (req, res) => {
  user.findAll({
    include: [gender, tarif, city, messenger]
  })
    .then(users => { res.json(users) })
    .catch(err => console.log(err))
})
app.post('/users', authenticateJWT, (req, res) => {
  user.create(req.body)
    .then(() => { res.json({ status: 201 }) })
    .catch(err => console.log(err));
})
app.put('/users/:id', authenticateJWT, async (req, res) => {
  user.findOne({where: {id: req.params.id}})
    .then(itemUser => {
      messenger.findAll({where: {id: req.body.messengerId}})
        .then(resMessenger => {
          itemUser.setMessengers(resMessenger)
        })
    })

  user.update(req.body, {
    where: {
      id: req.params.id
    }
  }).then(() => { res.json({status: 200})})
    .catch(err => console.log(err));
})
app.delete('/users/:id', authenticateJWT, (req, res) => {
  user.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => { res.json({status: 200})})
    .catch(err => console.log(err));
})

app.get('/tarif', authenticateJWT, (req, res) => {
  tarif.findAll()
    .then(result => { res.json(result) })
    .catch(err => console.log(err))
})
app.post('/tarif', authenticateJWT, (req, res) => {
  tarif.create(req.body)
    .then(() => { res.json({ status: 201 }) })
    .catch(err => console.log(err));
})
app.put('/tarif/:id', authenticateJWT, (req, res) => {
  tarif.update(req.body, {
    where: {
      id: req.params.id
    }
  }).then(() => { res.json({status: 200})})
    .catch(err => console.log(err));
})
app.delete('/tarif/:id', authenticateJWT, (req, res) => {
  tarif.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => { res.json({status: 200})})
    .catch(err => console.log(err));
})

app.get('/messenger', authenticateJWT, (req, res) => {
  messenger.findAll()
    .then(result => { res.json(result) })
    .catch(err => console.log(err))
})
app.post('/messenger', authenticateJWT, (req, res) => {
  messenger.create(req.body)
    .then(() => { res.json({ status: 201 }) })
    .catch(err => console.log(err));
})
app.put('/messenger/:id', authenticateJWT, (req, res) => {
  messenger.update(req.body, {
    where: {
      id: req.params.id
    }
  }).then(() => { res.json({status: 200})})
    .catch(err => console.log(err));
})
app.delete('/messenger/:id', authenticateJWT, (req, res) => {
  messenger.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => { res.json({status: 200})})
    .catch(err => console.log(err));
})

app.get('/city', authenticateJWT, (req, res) => {
  city.findAll()
    .then(result => { res.json(result) })
    .catch(err => console.log(err))
})
app.post('/city', authenticateJWT, (req, res) => {
  city.create(req.body)
    .then(() => { res.json({ status: 201 }) })
    .catch(err => console.log(err));
})
app.put('/city/:id', authenticateJWT, (req, res) => {
  city.update(req.body, {
    where: {
      id: req.params.id
    }
  }).then(() => { res.json({status: 200})})
    .catch(err => console.log(err));
})
app.delete('/city/:id', authenticateJWT, (req, res) => {
  city.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => { res.json({status: 200})})
    .catch(err => console.log(err));
})

app.get('/gender', authenticateJWT, (req, res) => {
  gender.findAll()
    .then(result => { res.json(result) })
    .catch(err => console.log(err))
})

app.get('/export', authenticateJWT, (req, res) => {
  user.findAll({
    include: [gender, tarif, city, messenger]
  })
    .then(result => {
      const wb = new xl.Workbook()
      const ws = wb.addWorksheet('Sheet 1', {})

      const header = ['id', 'fio', 'age', 'phone']
      for (let i = 0; i < header.length; i++) {
        ws.cell(1, i+1).string(header[i])
      }

      for (let i = 0; i < result.length; i++) {
        for (let j = 0; j < header.length; j++) {
          if (!result[i][header[j]]) return
          switch (typeof result[i][header[j]]) {
            case 'string':
              ws.cell(i+2, j+1).string(result[i][header[j]])
              break
            case 'number':
              ws.cell(i+2, j+1).number(result[i][header[j]])
              break
          }
        }
      }
      wb.write('export.xlsx', res);

    })
    .catch(err => console.log(err))
})

app.get('/statistic-export', authenticateJWT, (req, res) => {
  const wb = new xl.Workbook()
  const ws = wb.addWorksheet('Sheet 1', {})

  const header = ['id', 'age', 'otherCity', 'message', 'cluster']
  for (let i = 0; i < header.length; i++) {
    ws.cell(1, i + 1).string(header[i])
  }

  for (let i = 0; i < lastClusterData.length; i++) {
    for (let j = 0; j < header.length; j++) {
      if (lastClusterData[i][header[j]] || lastClusterData[i][header[j]] === 0) {
        switch (typeof lastClusterData[i][header[j]]) {
          case 'string':
            ws.cell(i + 2, j + 1).string(lastClusterData[i][header[j]])
            break
          case 'number':
            ws.cell(i + 2, j + 1).number(lastClusterData[i][header[j]])
            break
        }
      }
    }
  }
  wb.write('export.xlsx', res);
})

app.get('/statistic', authenticateJWT, (req, res) => {
  statistic.findAll()
    .then(result => { res.json(result) })
    .catch(err => console.log(err))
})

app.get('/statistic-cluster', authenticateJWT, (req, res) => {
  statistic.findAll()
    .then(data => {
      const cloneData = cloneDeep(data.map(a => a.dataValues))
      const normData = []

      const ageMean = mean(cloneData.map(a => a.age))
      const ageSd = sd(cloneData.map(a => a.age))
      const otherCityMean = mean(cloneData.map(a => a.otherCity))
      const otherCitySd = sd(cloneData.map(a => a.otherCity))
      const messageMean = mean(cloneData.map(a => a.message))
      const messageSd = sd(cloneData.map(a => a.message))
      for (let i = 0 ; i < cloneData.length ; i++) {
        normData[i] = {}
        normData[i]['age'] = (cloneData[i]['age'] - ageMean) / ageSd
        normData[i]['otherCity'] = (cloneData[i]['otherCity'] - otherCityMean) / otherCitySd
        normData[i]['message'] = (cloneData[i]['message'] - messageMean) / messageSd
      }

      let vectors = []
      for (let i = 0 ; i < normData.length ; i++) {
        vectors[i] = [ normData[i]['age'], normData[i]['otherCity'], normData[i]['message']];
      }

      const colorList = [
        'rgb(255, 99, 132)',
        'rgb(99,133,255)',
        'rgb(3,127,0)',
        'rgb(255,221,99)',
        'rgb(99,255,219)',
        'rgb(133,255,99)'
      ]

      kmeans.clusterize(vectors, {k: 3}, (errCluster, resCluster) => {
        if (errCluster) {
          console.error(errCluster);
        } else {
          resCluster.forEach((cluster, clusterId) => {
            cluster.clusterInd.forEach(index => {
              cloneData[index].cluster = clusterId
            })
          })
          lastClusterData = cloneData
          const groupCluster = groupBy(cloneData, 'cluster')
          const arr = []
          Object.keys(groupCluster).forEach(i => {
            arr.push({
              label: i + ' кластер',
              backgroundColor: colorList[i],
              data: groupCluster[i].map(item => {
                return {
                  r: item.age / 10,
                  x: item.otherCity,
                  y: item.message,
                }
              })
            })
          })

          res.json(arr)
        }
      });
    })
    .catch(err => console.log(err))
})
